/**
 * Created by Tom on 14-07-16.
 */
/*******************************************/
/****    ALKA INTRANET NOTIF	        ****/
/*******************************************/


function notify(title,content,type,time){
    if(!jQuery('#alka-notif-center').length){
        jQuery('body').append('<div id="alka-notif-center-container" style="position: fixed; bottom:10px; right: 10px; box-sizing: border-box; width: 100%;" class="container-fluid"><div id="alka-notif-center-row" class="row-fluid"><div id="alka-notif-center" class="span3"></div></div></div>');
    }
    var id = (new Date().getTime() + Math.floor((Math.random()*10000)+1)).toString(16);
    var notif = '<div class="notif" id="notif-'+id+'">'+
        '<div class="alert alert-'+type+'">';
    if(!time){
        notif 			+='<button type="button" class="close" onclick="javascript:notifyClear(\''+id+'\')">×</button>';
    }
    notif 			+='<h4 class="notif-title">'+title+'</h4>'+
        '<div class="notif-content">'+content+'</div>'+
        '</div>'+
        '</div>';
    jQuery(notif).appendTo("#alka-notif-center").hide().show('slide',{direction:"right"});
    if(time){
        window.setTimeout(function(){
            notifyClear(id);
        }, time);
    }
}

function notifyClear(id){
    if(id){
        jQuery('#notif-'+id).hide('slide',{direction:"right"},function(){
            jQuery('#notif-'+id).remove();
        });
    }else{
        jQuery('#alka-notif-center .notif').hide('slide',{direction:"right"},function(){
            jQuery('#alka-notif-center .notif').remove();
        });
    }
}
