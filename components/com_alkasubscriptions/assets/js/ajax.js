/**
 * Created by Tom on 28-07-17.
 */
jQuery(document).ready(function(){

   jQuery('#set-address-form').submit(function(event){
        event.preventDefault();
        var datas = jQuery('#set-address-form').serializeArray();

        jQuery.ajax({
            url:'index.php',
            dataType:'json',
            method:'post',
            data : datas,

            success:function(result){
               jQuery('#set-address-modal').modal('hide');
              // var actualSearch  = URI(window.location).search(true);
              // actualSearch.tab = "addresses";
              // window.location.href = URI(window.location).search(actualSearch).toString();
               location.reload();
               console.log('save');
            }
        });
    });

});

function addSubscription(){
  jQuery('#add-subscription-modal').modal('show');
}

function setAddress(idAddress){
  jQuery('#link-address-modal').modal('hide');
  if(idAddress){
    jQuery.ajax({
        url:'index.php',
        dataType:'json',
        method:'get',
        data : 'option=com_alkasubscriptions&controller=addresses&task=getAddress&idAddress='+idAddress,

        success:function(result){
            jQuery('#addresses_id').val(result.data.id);
            jQuery('#addresses_title').val(result.data.title);
            jQuery('#addresses_firstname').val(result.data.firstname);
            jQuery('#addresses_lastname').val(result.data.lastname);
            jQuery('#addresses_company').val(result.data.company);
            jQuery('#addresses_addressLine1').val(result.data.addressLine1);
            jQuery('#addresses_addressLine2').val(result.data.addressLine2);
            jQuery('#addresses_city').val(result.data.city);
            jQuery('#addresses_country').val(result.data.country);
            jQuery('#addresses_zip').val(result.data.zip);
            jQuery('#addresses_note').val(result.data.note);
            jQuery('#addresses_enabled').val(result.data.enabled);
        }
    });

  }else{
    jQuery('#set-address-form')[0].reset();
  }
  jQuery('#set-address-modal').modal('show');
}

function linkAddressShow(idSubscription, idAddress){
  jQuery('#btn-link-' + idAddress).removeClass('btn-primary').addClass('btn-warning').attr('disabled','disabled');
  jQuery('#link-address-modal').attr('data-idsubscription', idSubscription);
  jQuery('#link-address-modal').attr('data-idaddress', idAddress);
  jQuery('#link-address-modal').modal('show');
}

function linkAddress(idAddress){
  idSubscription = jQuery('#link-address-modal').attr('data-idsubscription');

  jQuery.ajax({
      url:'index.php',
      dataType:'json',
      method:'post',
      data : 'option=com_alkasubscriptions&controller=subscriptions&task=linkAddress&idSubscription='+idSubscription+'&idAddress='+idAddress,

      success:function(result){
         jQuery('#link-address-modal').modal('hide');
         var actualSearch  = URI(window.location).search(true);
         actualSearch.tab = "subscriptions";
         window.location.href = URI(window.location).search(actualSearch).toString();
      }
  });
}

function checkLink(idAddress){
  jQuery.ajax({
      url:'index.php',
      dataType:'json',
      method:'post',
      data : 'option=com_alkasubscriptions&controller=addresses&task=checkLink&idAddress='+idAddress,

      success:function(result){
        if(result.data == true){
          // Address linked to subscription => can't disable
          notify(result.message.title, result.message.body, 'danger', 10000);
        } else {
          // Address not linked => disable
          disableAddress(idAddress);
        }
      }
  });
}

function disableAddress(idAddress){
  jQuery.ajax({
      url:'index.php',
      dataType:'json',
      method:'post',
      data : 'option=com_alkasubscriptions&controller=addresses&task=disable&idAddress='+idAddress,

      success:function(result){
        var actualSearch  = URI(window.location).search(true);
        actualSearch.tab = "addresses";
        window.location.href = URI(window.location).search(actualSearch).toString();
      }
  });
}
