<?php

/**
 * @version    CVS: 1.0.0
 * @package    com_alkasubscriptions
 * @author     Alka <developpement@alkaonline.be>
 * @copyright  2017 Alka
 * @license    GNU General Public License version 2 ou version ultérieure ; Voir LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Class AlkaSubscriptionsController
 *
 * @since  1.0
 */
class AlkaSubscriptionsControllerSubscriptions extends JControllerLegacy
{
    public function linkAddress(){
      $idAddress = JFactory::getApplication()->input->get('idAddress', null);
      $idSubscription = JFactory::getApplication()->input->get('idSubscription', null);

      $db = JFactory::getDbo();
      $query = $db->getQuery(true);

      $fields = array(
        $db->quoteName('idAddress') . ' = ' . $idAddress
      );

      $conditions = array(
          $db->quoteName('id') . ' = '. $idSubscription
      );

      $query->update($db->quoteName('#__alkasubscriptions_subscription'))->set($fields)->where($conditions);

      $db->setQuery($query);

      $result = $db->execute();

      //JFactory::getApplication()->redirect('index.php?option=com_alkasubscriptions&view=subscriptions&Itemid='.JFactory::getApplication()->input->get('Itemid', null));

      $message['title'] = "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>". ' ' . JText::_('COM_ALKASUBSCRIPTIONS_SUCCESS');
      $message['body'] = str_replace('[n]', $subscription->name,JText::_('COM_ALKASUBSCRIPTIONS_LINKED_ADDRESS_SUCCESSFULLY'));

      echo new JResponseJson(true, $message);
      die();
    }
}
