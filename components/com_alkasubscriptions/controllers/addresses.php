<?php

/**
 * @version    CVS: 1.0.0
 * @package    com_alkasubscriptions
 * @author     Alka <developpement@alkaonline.be>
 * @copyright  2017 Alka
 * @license    GNU General Public License version 2 ou version ultérieure ; Voir LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Class AlkaSubscriptionsController
 *
 * @since  1.0
 */
class AlkaSubscriptionsControllerAddresses extends JControllerLegacy
{
    public function setAddress(){
        $address = (object) JFactory::getApplication()->input->get('addresses',null,null);
        $user = JFactory::getUser();
        $date = new DateTime();

        $address -> createdDate = $date->format('Y-m-d H:i:s');
        $address -> createdBy   = $user->id;

        $db = JFactory::getDbo();
        $result = AlkaSubscriptionsHelper::setAddress(
          $address->id,

          $address->title,
          $address->firstname,
          $address->lastname,
          $address->company,
          $address->addressLine1,
          $address->addressLine2,
          $address->city,
          $address->zip,
          $address->country,
          $address->enabled,
          $address->note,
          $address->hikaAddressId,
          $address->idUser);
        if($address->id){
          $message['title'] = "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>". ' ' . JText::_('COM_ALKASUBSCRIPTIONS_SUCCESS');
          $message['body'] = str_replace('[n]', $subscription->name, JText::_('COM_ALKASUBSCRIPTION_ADDRESS_UPDATED'));
        } else {
          $message['title'] = "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>". ' ' . JText::_('COM_ALKASUBSCRIPTIONS_SUCCESS');
          $message['body'] = str_replace('[n]', $subscription->name, JText::_('COM_ALKASUBSCRIPTION_ADDRESS_CREATED'));
        }

        echo new JResponseJson(true ,$message);
        die();
    }

    public function getAddress(){
      $idAddress = JFactory::getApplication()->input->get('idAddress', null);

      $db     = JFactory::getDBO();
      $query  = $db->getQuery(true);
      $query->select(
          array(
              $db->quoteName('#__alkasubscriptions_address').'.*',
          )
      );

      $query->from($db->quoteName('#__alkasubscriptions_address'));
      $query->where($db->quoteName('id') . ' = ' . $idAddress);
      $db->setQuery((string) $query);
      $items = $db->loadObject();
      echo new JResponseJson($items,$message);
      die();
    }

    // Check if address is linked to a subscription (ex: before disable)
    public function checkLink(){
      $idAddress = JFactory::getApplication()->input->get('idAddress', null);

      $db     = JFactory::getDBO();
      $query  = $db->getQuery(true);
      $query->select($db->quoteName('name'));
      $query->from($db->quoteName('#__alkasubscriptions_subscription'));
      $query->where($db->quoteName('idAddress') . ' = '. $idAddress);
      $db->setQuery((string) $query);

      $result = $db->loadObject();

      if ($result !== null){
        // Address linked to subscription => can't disable
        $message['title'] = "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>". ' ' . JText::_('COM_ALKASUBSCRIPTIONS_ADDRESS_DISABLE_FAIL');
        $message['body'] = str_replace('[abo]', $result->name,JText::_('COM_ALKASUBSCRIPTIONS_ADDRESS_CANNOT_DISABLE'));
        echo new JResponseJson(true , $message);
        die();
      } else {
        // Address not linked => disable
        $message['title'] = "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>". ' ' . JText::_('COM_ALKASUBSCRIPTIONS_SUCCESS');
        $message['body'] = str_replace('[abo]', $result->name,JText::_('COM_ALKASUBSCRIPTIONS_ADDRESS_DISABLED'));
        echo new JResponseJson(false , $message);
        die();
      }
    }

    public function disable(){
      $id = JFactory::getApplication()->input->get('idAddress', null);

      $db = JFactory::getDbo();
      $query = $db->getQuery(true);

      $fields = array(
        $db->quoteName('enabled') . ' = 0'
      );

      $conditions = array(
          $db->quoteName('id') . ' = '. $id
      );

      $query->update($db->quoteName('#__alkasubscriptions_address'))->set($fields)->where($conditions);

      $db->setQuery($query);

      $result = $db->execute();

      $message['title'] = "<i class=\"fa fa-check\" aria-hidden=\"true\"></i>". ' ' . JText::_('COM_ALKASUBSCRIPTIONS_SUCCESS');
      $message['body'] = str_replace('[abo]', $result->name,JText::_('COM_ALKASUBSCRIPTIONS_ADDRESS_DISABLED'));

      echo new JResponseJson(true , $message);
      die();
    }

}
