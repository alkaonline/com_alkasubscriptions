<?php
/**
 * @version    CVS: 1.0.0
 * @package    com_alkasubscriptions
 * @author     Alka <developpement@alkaonline.be>
 * @copyright  2017 Alka
 * @license    GNU General Public License version 2 ou version ultérieure ; Voir LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for the HelloWorld Component
 *
 * @since  0.0.1
 */
class AlkaSubscriptionsViewSubscriptions extends JViewLegacy
{
	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		// Load scripts
		$document = JFactory::getDocument();
		JHtml::_('jquery.framework');
		$document->addScript('https://code.jquery.com/ui/1.9.2/jquery-ui.min.js');
		//$document->addScript('https://code.jquery.com/jquery-3.2.1.min.js');
		//$document->addScript('https://code.jquery.com/ui/1.9.2/jquery-ui.min.js');
		$document->addScript('components/com_alkasubscriptions/assets/js/URI.min.js');
		$document->addScript('components/com_alkasubscriptions/assets/js/notifications.js');
		$document->addScript('components/com_alkasubscriptions/assets/js/ajax.js');

		$document->addStyleSheet('components/com_alkasubscriptions/assets/css/style.css');

		// Assign data to the view
		$this->subscriptions = $this->get('Subscriptions');
		$this->addresses = $this->get('Addresses');
        $this->productsToExtend = $this->get('ProductsToExtend');


		// Load forms
		$this->formSubscription     = JForm::getInstance('subscriptions', JPATH_COMPONENT.'/models/forms/subscriptions.xml');
		$this->formAddress     			= JForm::getInstance('addresses', JPATH_COMPONENT.'/models/forms/addresses.xml');
		$this->formLinkAddress     	= JForm::getInstance('linkaddresses', JPATH_COMPONENT.'/models/forms/linkaddresses.xml');

		// Display the view
		parent::display($tpl);
	}
}
