<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HelloWorlds View
 *
 * @since  0.0.1
 */
class AlkaSubscriptionsViewSubscriptionsOriginal extends JViewLegacy
{
	/**
	 * Display the Hello World view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		$document = JFactory::getDocument();
		// Load JS files
		JHtml::_('jquery.framework');
		$document->addScript('https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js');
		$document->addScript('https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js');
		$document->addScript('https://cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js');
		$document->addScript('https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js');
		$document->addScript('https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js');

		$document->addScript('components/com_alkasubscriptions/assets/js/URI.min.js');
		$document->addScript('components/com_alkasubscriptions/assets/js/notifications.js');
		$document->addScript('components/com_alkasubscriptions/assets/js/ajax.js');

		// Load CSS files
		$document->addStyleSheet('components/com_alkasubscriptions/assets/css/style.css');
		$document->addStyleSheet('https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css');
		$document->addStyleSheet('https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css');
		// Assign data to the view
		$this->subscriptions 	= $this->get('Subscriptions');
		$this->addresses 			= $this->get('Addresses');

		// Load forms
		$this->formAddress    = JForm::getInstance('addresses', JPATH_COMPONENT.'/models/forms/addresses.xml');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolBar()
	{
		JToolBarHelper::title(JText::_('COM_ALKASUBSCRIPTIONS'). ' Original');
		JToolBarHelper::preferences('com_alkasubscriptions');
        //JToolBarHelper::custom('export.subscriptions', 'rebuild', 'rebuild', 'Export subscriptions', false);
	}
}
