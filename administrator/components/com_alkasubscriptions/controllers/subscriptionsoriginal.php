<?php

/**
 * @version    CVS: 1.0.0
 * @package    com_alkasubscriptions
 * @author     Alka <developpement@alkaonline.be>
 * @copyright  2017 Alka
 * @license    GNU General Public License version 2 ou version ultérieure ; Voir LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Class AlkaSubscriptionsController
 *
 * @since  1.0
 */
class AlkaSubscriptionsControllerSubscriptionsOriginal extends JControllerLegacy
{
    public function setState(){
        $subscription = (object) JFactory::getApplication()->input->get('subscriptions',null,null);

        $id = JFactory::getApplication()->input->get('id', null);
        $state = (int)JFactory::getApplication()->input->get('state', null);

        $currentUserID = JFactory::getUser()->id;
        $nowDate = new DateTime();

        $enabled = null;

        // Get a db connection
        $db = JFactory::getDbo();

        // Create a new query object
        $query = $db->getQuery(true);

        if($state == 1){
          $enabled = 0;
        } else if($state == 0){
          $enabled = 1;
        }

        if($id){
          // Update query

          // Fields to update
          $fields = array(
              $db->quoteName('enabled') . ' = ' . $enabled,
              $db->quoteName('modifiedDate') . ' = ' . $db->quote($nowDate->format('Y-m-d H:i:s')),
              $db->quoteName('modifiedBy') . ' = ' . $currentUserID
          );

          // Conditions for which records should be updated
          $conditions = array(
              $db->quoteName('id') . ' = ' . $id
          );

          $query->update($db->quoteName('#__alkasubscriptions_subscription'))->set($fields)->where($conditions);

          $db->setQuery($query);

          $result = $db->execute();

          if($result){
            JFactory::getApplication()->redirect('administrator/index.php?option=com_alkasubscriptions');
            return $id;
          } else {
            echo '<div class="alert alert-warning">Update failed.</div>';
            return false;
          }
      }
    }

    public function setNotif(){
        $subscription = (object) JFactory::getApplication()->input->get('subscriptions',null,null);

        $id = JFactory::getApplication()->input->get('id', null);
        $state = (int)JFactory::getApplication()->input->get('state', null);

        $currentUserID = JFactory::getUser()->id;
        $nowDate = new DateTime();

        $enabled = null;

        // Get a db connection
        $db = JFactory::getDbo();

        // Create a new query object
        $query = $db->getQuery(true);

        if($state == 1){
          $notif = 0;
        } else if($state == 0){
          $notif = 1;
        }

        if($id){
          // Update query

          // Fields to update
          $fields = array(
              $db->quoteName('sendNotification') . ' = ' . $notif,
              $db->quoteName('modifiedDate') . ' = ' . $db->quote($nowDate->format('Y-m-d H:i:s')),
              $db->quoteName('modifiedBy') . ' = ' . $currentUserID
          );

          // Conditions for which records should be updated
          $conditions = array(
              $db->quoteName('id') . ' = ' . $id
          );

          $query->update($db->quoteName('#__alkasubscriptions_subscription'))->set($fields)->where($conditions);

          $db->setQuery($query);

          $result = $db->execute();

          if($result){
            JFactory::getApplication()->redirect('administrator/index.php?option=com_alkasubscriptions');
            return $id;
          } else {
            echo '<div class="alert alert-warning">Update failed.</div>';
            return false;
          }
      }
    }

    public function linkAddress(){
  		$idAddress = JFactory::getApplication()->input->get('idAddress', null);
  		$idSubscription = JFactory::getApplication()->input->get('idSubscription', null);

  		$db = JFactory::getDbo();
  		$query = $db->getQuery(true);

  		$fields = array(
  			$db->quoteName('idAddress') . ' = ' . $idAddress
  		);

  		$conditions = array(
  				$db->quoteName('id') . ' = '. $idSubscription
  		);

  		$query->update($db->quoteName('#__alkasubscriptions_subscription'))->set($fields)->where($conditions);

  		$db->setQuery($query);

  		$result = $db->execute();

  		echo new JResponseJson(true, $message);
  		die();
  	}
}
