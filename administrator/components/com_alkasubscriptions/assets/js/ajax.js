/**
 * Created by Tom on 28-07-17.
 */
jQuery(document).ready(function(){
    console.log("test"+jQuery('#subscriptions-table').length);
    if(jQuery('#subscriptions-table').length>0){
        jQuery('#subscriptions-table').DataTable( {
         order: [[ 3, "desc" ]],
         dom: 'Bfrtip',
         buttons: [
             'csv', 'excel'
         ]
        });
    }

    if(jQuery('#addresses-table').length>0){
        jQuery('#addresses-table').DataTable( {
        dom: 'Bfrtip',
        buttons: [
           'csv', 'excel'
        ]
        });
    }

    if(jQuery('#addresses-modal-table').length>0){
      jQuery('#addresses-modal-table').DataTable( {
       dom: 'Bfrtip',
       buttons: [
           'csv', 'excel'
       ]
      });
    }
  jQuery('#add-subscription-form').submit(function(event){
       event.preventDefault();
       var datas = jQuery('#add-subscription-form').serializeArray();

       jQuery.ajax({
           url:'index.php',
           dataType:'json',
           method:'post',
           data : datas,

           success:function(msg){
              jQuery('#add-subscription-modal').modal('hide');
              var actualSearch  = URI(window.location).search(true);
              actualSearch.tab = "subscriptions";
              window.location.href = URI(window.location).search(actualSearch).toString();
           }
       });
   });

   jQuery('#set-address-form').submit(function(event){
        event.preventDefault();
        var datas = jQuery('#set-address-form').serializeArray();

        jQuery.ajax({
            url:'index.php',
            dataType:'json',
            method:'post',
            data : datas,

            success:function(result){
              jQuery('#set-address-modal').modal('hide');
              var actualSearch  = URI(window.location).search(true);
              actualSearch.tab = "addresses";
              window.location.href = URI(window.location).search(actualSearch).toString();
            }
        });
    });
});

function addSubscription(){
  jQuery('#add-subscription-modal').modal('show');
}

function setAddress(idAddress){
  jQuery('#link-address-modal').modal('hide');
  console.log(idAddress);
  if(idAddress){
    jQuery.ajax({
        url:'index.php',
        dataType:'json',
        method:'get',
        data : 'option=com_alkasubscriptions&controller=addresses&task=getAddress&idAddress='+idAddress,

        success:function(result){
            jQuery('#addresses_id').val(result.data.id);
            jQuery('#addresses_title').val(result.data.title);
            jQuery('#addresses_firstname').val(result.data.firstname);
            jQuery('#addresses_lastname').val(result.data.lastname);
            jQuery('#addresses_company').val(result.data.company);
            jQuery('#addresses_addressLine1').val(result.data.addressLine1);
            jQuery('#addresses_addressLine2').val(result.data.addressLine2);
            jQuery('#addresses_city').val(result.data.city);
            jQuery('#addresses_country').val(result.data.country);
            jQuery('#addresses_zip').val(result.data.zip);
            jQuery('#addresses_note').val(result.data.note);
            jQuery('#addresses_enabled').val(result.data.enabled);
        }
    });
  }else{
    jQuery('#set-address-form')[0].reset();
  }
  jQuery('#set-address-modal').modal('show');
}

function linkAddressShow(idSubscription, idAddress){
  jQuery('#btn-link-' + idAddress).removeClass('btn-primary').addClass('btn-warning').attr('disabled','disabled');
  jQuery('#link-address-modal').attr('data-idsubscription', idSubscription);
  jQuery('#link-address-modal').attr('data-idaddress', idAddress);
  jQuery('#link-address-modal').modal('show');
}

function linkAddress(idAddress){
  idSubscription = jQuery('#link-address-modal').attr('data-idsubscription');
  console.log('idAddress: ' + idAddress  +' idSubscription: '+ idSubscription);
  jQuery.ajax({
      url:'index.php',
      dataType:'json',
      method:'post',
      data : 'option=com_alkasubscriptions&controller=subscriptions&task=linkAddress&idSubscription='+idSubscription+'&idAddress='+idAddress,

      success:function(msg){
        jQuery('#link-address-modal').modal('hide');
        var actualSearch  = URI(window.location).search(true);
        actualSearch.tab = "subscriptions";
        window.location.href = URI(window.location).search(actualSearch).toString();
      }
  });
}

function checkLink(idAddress){
  jQuery.ajax({
      url:'index.php',
      dataType:'json',
      method:'post',
      data : 'option=com_alkasubscriptions&controller=addresses&task=checkLink&idAddress='+idAddress,

      success:function(result){
        if(result.data == true){
          notify(result.message.title, result.message.body, 'danger', 10000);
        } else {
          setAddressState(idAddress, 0);
        }
      }
  });
}

function setAddressState(idAddress, state){
  jQuery.ajax({
    url:'index.php',
    dataType:'json',
    method:'post',
    data : 'option=com_alkasubscriptions&controller=addresses&task=setAddressState&idAddress='+idAddress+'&state='+state,

      success:function(result){

        if(result.data == true){
          var actualSearch  = URI(window.location).search(true);
          actualSearch.tab = "addresses";
          window.location.href = URI(window.location).search(actualSearch).toString();
        } else {
          notify(result.message.title, result.message.body, 'danger', 10000);
        }
      }
  });
}

function trashedSub(idSub){
  if(confirm('Confirmer la suppression ?')){
    jQuery.ajax({
      url:'index.php',
      dataType:'json',
      method:'post',
      data : 'option=com_alkasubscriptions&controller=subscriptions&task=trashSub&idSub='+idSub,
      success:function(result){
        jQuery('#row-'+idSub).hide(1000, function(){

            jQuery('#row-'+idSub).remove();
        });
      }
    });
  }
}


function prepareDateForm(start, end, subId){
    jQuery('#subscription_id').val(subId);

    var startCalendar = document.getElementById('subscription_startDate').parentElement.parentElement._joomlaCalendar;
    startCalendar.date = (new Date(start));
    startCalendar.callHandler();

    var endCalendar = document.getElementById('subscription_endDate').parentElement.parentElement._joomlaCalendar;
    endCalendar.date = (new Date(end));
    endCalendar.callHandler();
    jQuery('#set-date-modal').modal('show');
}
function submitDateForm(){
    var subId = jQuery('#subscription_id').val();
    var startCalendar = document.getElementById('subscription_startDate').parentElement.parentElement._joomlaCalendar;
    var endCalendar = document.getElementById('subscription_endDate').parentElement.parentElement._joomlaCalendar;
    if(startCalendar.date > endCalendar.date){
        alert('End date is anterior to start date !');
    }else{
        jQuery('#set-date-form .modal-footer a, #set-date-form .modal-footer btn').prop('disabled',true).addClass('disabled');
        jQuery.ajax({
          url:'index.php',
          dataType:'json',
          method:'post',
          data : jQuery('#set-date-form').serialize(),
            success:function(result){
                jQuery('#startDate-'+subId).html('<a href="javascript:void(0);" onclick="prepareDateForm(\''+result.message.startDate+'\', \''+result.message.endDate+'\','+subId+' );">'+result.message.hStartDate+'</a>');
                jQuery('#endDate-'+subId).html('<a href="javascript:void(0);" onclick="prepareDateForm(\''+result.message.startDate+'\', \''+result.message.endDate+'\','+subId+' );">'+result.message.hEndDate+'</a>');
                jQuery('#set-date-modal').modal('show');
                jQuery('#set-date-form .modal-footer a, #set-date-form .modal-footer btn').prop('disabled',false).removeClass('disabled');
                jQuery('#set-date-modal').modal('hide');
            }, error:function(){
                alert('Problem');
                jQuery('#set-date-form .modal-footer a, #set-date-form .modal-footer btn').prop('disabled',false).removeClass('disabled');
            }
        });
    }
}

function prepareIssueForm(start, end, subId){
    jQuery('#subscription_issues_id').val(subId);

    var startIssue = jQuery('#subscription_issues_startIssue').val(start);
    //jQuery('#subscription_issues_startIssue').attr('max',start);

    var endIssue = jQuery('#subscription_issues_endIssue').val(end);
    //jQuery('#subscription_issues_endIssue').attr('min',end);
    jQuery('#set-issue-modal').modal('show');
}
function submitIssueForm(){
    var subId = jQuery('#subscription_issues_id').val();
    var start = jQuery('#subscription_issues_startIssue').val();
    var end = jQuery('#subscription_issues_endIssue').val();
    if(start > end){
        alert('Last issue is higher than firrst issue !');
    }else{
        jQuery('#set-issue-form .modal-footer a, #set-issue-form .modal-footer btn').prop('disabled',true).addClass('disabled');
        jQuery.ajax({
          url:'index.php',
          dataType:'json',
          method:'POST',
          data : jQuery('#set-issue-form').serialize(),
            success:function(result){
                jQuery('#startIssue-'+subId).html('<a href="javascript:void(0);" onclick="prepareIssueForm(\''+result.message.startIssueNumber+'\', \''+result.message.endIssueNumber+'\','+subId+' );">'+result.message.startIssueNumber+'</a>');
                jQuery('#endIssue-'+subId).html('<a href="javascript:void(0);" onclick="prepareIssueForm(\''+result.message.startIssueNumber+'\', \''+result.message.endIssueNumber+'\','+subId+' );">'+result.message.endIssueNumber+'</a>');
                jQuery('#set-issue-modal').modal('show');
                jQuery('#set-issue-form .modal-footer a, #set-issue-form .modal-footer btn').prop('disabled',false).removeClass('disabled');
                jQuery('#set-issue-modal').modal('hide');
            }, error:function(){
                alert('Problem');
                jQuery('#set-issue-form .modal-footer a, #set-issue-form .modal-footer btn').prop('disabled',false).removeClass('disabled');
            }
        });
    }
}
