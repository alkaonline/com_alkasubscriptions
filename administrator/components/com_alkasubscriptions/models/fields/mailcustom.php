<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Alkaintranet
 * @author     Alka <developpement@alkaonline.be>
 * @copyright  2016 Alka
 * @license    GNU General Public License version 2 ou version ultérieure ; Voir LICENSE.txt
 */

defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

class JFormFieldMailcustom extends JFormField{

	protected $type = 'mailcustom';
	protected function getInput()
	{
		$editor = JFactory::getEditor('codemirror');

		$value = (!empty($this->value))? $this->value : file_get_contents(JPATH_SITE.'/components/com_alkasubscriptions/assets/mails/basic/first_alert.html');
    // print_r($value);
		// die();
		$html = $editor->display($this->name,$value , "800", "100", "150", "30", 1, null, null, null, array('mode' => 'advanced','filter'=>'RAW'));
		//$html .='<div class="help-block" style="margin-top:20px;"><div class="alert alert-info">'.JText::_('COM_ALKAINTRANET_CUSTOM_MAIL_HELPER').'</div></div>';
		return $html;
	}
}
