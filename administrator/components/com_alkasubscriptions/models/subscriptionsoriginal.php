<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HelloWorldList Model
 *
 * @since  0.0.1
 */
class AlkaSubscriptionsModelSubscriptionsOriginal extends JModelList
{
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	static public function getSubscriptions()
	{
		$currentUserID = JFactory::getUser()->id;

		$db     = JFactory::getDBO();
		$query  = $db->getQuery(true);

		$query->select(
				array(
						$db->quoteName('s.id','idSub'),
						$db->quoteName('s.name'),
						$db->quoteName('s.startDate'),
						$db->quoteName('s.endDate'),
						$db->quoteName('s.forever'),
						$db->quoteName('s.paid'),
						$db->quoteName('s.enabled'),
						$db->quoteName('s.idUser'),
						$db->quoteName('s.idOrder'),
						$db->quoteName('s.idProduct'),
                        $db->quoteName('s.sendNotification'),
						$db->quoteName('p.product_code','productCode'),
						$db->quoteName('s.productOptions'),
						$db->quoteName('a.id','idAdd'),
						$db->quoteName('a.firstname'),
						$db->quoteName('a.lastname'),
						$db->quoteName('a.company'),
						$db->quoteName('a.addressLine1'),
						$db->quoteName('a.addressLine2'),
						$db->quoteName('a.zip'),
						$db->quoteName('a.city'),
						$db->quoteName('a.country'),
						$db->quoteName('u.username'),
						'GROUP_CONCAT('.$db->quoteName('g.title').' SEPARATOR \';\') AS groups',
				)
		);

		$query->from($db->quoteName('#__alkasubscriptions_subscription', 's'));

		$query->join('LEFT', $db->quoteName('#__alkasubscriptions_address', 'a')
		. ' ON (' .
		$db->quoteName('a.id')
		. ' = ' .
		$db->quoteName('s.idAddress') . ')');

		$query->join('LEFT', $db->quoteName('#__users', 'u')
		. ' ON (' .
		$db->quoteName('s.idUser')
		. ' = ' .
		$db->quoteName('u.id') . ')');

		$query->join('LEFT', $db->quoteName('#__hikashop_product', 'p')
		. ' ON (' .
		$db->quoteName('s.idProduct')
		. ' = ' .
		$db->quoteName('p.product_id') . ')');

		$query->join('LEFT', $db->quoteName('#__user_usergroup_map', 'gm')
		. ' ON (' .
		$db->quoteName('gm.user_id')
		. ' = ' .
		$db->quoteName('u.id') . ')');

		$query->join('LEFT', $db->quoteName('#__usergroups', 'g')
		. ' ON (' .
		$db->quoteName('g.id')
		. ' = ' .
		$db->quoteName('gm.group_id') . ')');
		$query->group($db->quoteName('s.id'));

		$db->setQuery((string) $query);
		$items = $db->loadObjectList();

		return $items;
	}

	static public function getAddresses()
	{
			$db     = JFactory::getDBO();
			$query  = $db->getQuery(true);
			$query->select(
					array(
							$db->quoteName('#__alkasubscriptions_address').'.*',
					)
			);
			$query->from($db->quoteName('#__alkasubscriptions_address'));
			$db->setQuery((string) $query);
			$items = $db->loadObjectList();
			return $items;
	}

}
