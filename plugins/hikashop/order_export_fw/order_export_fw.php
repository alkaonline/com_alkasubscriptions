<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.6.4
 * @author	alkaonline.be
 * @copyright	(C) 2010-2016 Alka. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
class plgHikashopOrder_export_fw extends JPlugin
{
	var $message = '';
	function __construct(&$subject, $config){
		parent::__construct($subject, $config);
	}

	function onBeforeOrderExport(&$rows){

		if(sizeof($rows)){ // If there's at least one entry
			foreach($rows as &$row){ //for each line in the export
				//Add bank communication in a distinct column.
				$row->bankcommunication = @unserialize($row->order_payment_params)->communication;
			}
		}

	}
}
