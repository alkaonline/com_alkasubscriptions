<?php

defined('_JEXEC') or die('Restricted access');
$params 	= JComponentHelper::getParams('com_alkasubscriptions');
$jinput = JFactory::getApplication()->input;
$document = JFactory::getDocument();
$showYellowBlock = false;
$renewPossibilities = array();
$renewPossibilitiesByCart = array();
$exceptions = array();

  foreach($activeSubscriptions as $activeSubscription){
    foreach($products as $product){
      if(in_array($product->product_id, $activeSubscription->canBeRenewWith)){
        for($i=1;$i<=$product->cart_product_quantity;$i++){
          $renewPossibilities[$activeSubscription->id][$product->product_id]=1;
          $renewPossibilitiesByCart[$product->product_id][]=$activeSubscription->id;
          if(sizeof($renewPossibilities[$activeSubscription->id]) > 1){
            //More than one product in the cart can be used to renew a sub
            $exceptions[] = $activeSubscription->id;
            $showYellowBlock=true;
          }
          if(sizeof($renewPossibilitiesByCart[$product->product_id])> 1){
            //More than one sub can be renew by a product in the cart
            $exceptions = array_merge($exceptions, $renewPossibilitiesByCart[$product->product_id]);
            $showYellowBlock=true;
          }
        }
      }
    }
  }
?>


<div id="automatch" ><!-- Vous êtes en train de renouveler un/des abonnement(s) --></div>
<div class="alert alert-warning" id="yellowBlock">
    <h2>Quel(s) abonnement(s) souhaitez-vous prolonger ?</h2>
    <p>Les produits de votre panier sont-ils destinés à prolonger un ou plusieurs abonnements en cours ? Si oui, sélectionnez les abonnements à prolonger en choisissant « Prolonger avec » dans le menu déroulant ci-dessous.</p>
    <table class="table">
        <head>
            <tr>
                <th>Vos abonnements actuels</th>
                <?php if($params->get('mode')=='duration'){ ?>
                    <th>Fin</th>
                <?php }else if($params->get('mode')=='issue'){ ?>
                    <th>Période</th>
                <?php } ?>
                <th>Prolonger</th>
            </tr>
        </head>
        <body>
            <?php foreach($activeSubscriptions as $activeSubscription){
                $document->addScriptDeclaration('
                 jQuery(document).ready(function(){
                     preLockAlkaRenewal();
                 });
                ');
                ?>
                <tr <?php if(!in_array($activeSubscription->id,$exceptions)){echo 'style="display:none;"';} ?> >
                    <td>Destinataire : <?php echo $activeSubscription->company.' '.$activeSubscription->firstname.' '.$activeSubscription->lastname.' - '.$activeSubscription->addressLine1.' '.$activeSubscription->addressLine2.' - '.$activeSubscription->zip.' '.$activeSubscription->city.' '.$activeSubscription->country; ?><br/><small><?php $dynamic =  AlkaSubscriptionsHelper::getSubscriptionNameDynamic($activeSubscription); echo $dynamic->name; ?></small></td>
                    <?php if($params->get('mode')=='duration'){ ?>
                        <td><?php $endDate = new DateTime($activeSubscription->endDate);  echo $endDate->format(JText::_('DATE_FORMAT_LC')) ; ?></td>
                    <?php }else if($params->get('mode')=='issue'){ ?>
                        <td style="white-space:nowrap">
                            <?php
                                $activeSubscription->startIssueDate = new DateTime($activeSubscription->startIssueDate);
                                $activeSubscription->endIssueDate = new DateTime($activeSubscription->endIssueDate);
                                echo $activeSubscription->startIssueNumber .', '.JText::_(strtoupper($activeSubscription->startIssueDate->format('F'))).' '.$activeSubscription->startIssueDate->format('Y').' -<br/>'.$activeSubscription->endIssueNumber.', '. JText::_(strtoupper($activeSubscription->endIssueDate->format('F'))).' '.$activeSubscription->endIssueDate->format('Y');
                            ?>
                        </td>
                        <td>

                            <?php
                             $precValues = $jinput->get('hikashop_alkasubscriptions_renew', array(),'array'); ?>
                            <select class="input-form alkasubscriptions-renews" id="hikashop_alkasubscriptions_renew_<?php echo $activeSubscription->id; ?>" name="hikashop_alkasubscriptions_renew[<?php echo $activeSubscription->id; ?>]" data-subscription-type="<?php echo $activeSubscription->subscriptionType; ?>" onchange="lockAlkaRenewal(jQuery(this),'<?php echo $activeSubscription->subscriptionType; ?>');">
                                <option value=""> Non (l'abonnement ci-dessous est un nouvel abonnement) </option>
                                <?php foreach($products as $product){
                                    if(in_array($product->product_id, $activeSubscription->canBeRenewWith)){

                                        for($i=1;$i<=$product->cart_product_quantity;$i++){ ?>
                                            <option data-product-in-cart-id="<?php echo $product->cart_product_id.'_'.$i; ?>" <?php if($precValues[$activeSubscription->id] == $product->product_id or ($product->cart_product_quantity>=1 && $i==1 && !in_array($activeSubscription->id,$exceptions))){echo 'selected="selected"';} ?> value="<?php echo $product->product_id; ?>" >Prolonger avec (<?php echo $product->product_name; ?>)</option>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    <?php } ?>
                </tr>
            <?php } ?>
        </body>
    </table>
    <!--<p>* Cette colonne représente les numéros que vous avez reçu ou que vous allez recevoir. <span class="label label-warning">XX</span> représente le dernier numéro envoyé aux abonnés.</p>
    <p>** La gestion des adresses de destinations se fait après la commande. Vous avez la possibilité de modifier l'adresse de destination dans la page "Votre compte" accessible en dehors du processus de commande.</p>-->
</div>
<?php
JHtml::_('jquery.framework');
JHtml::_('jquery.ui');
$document->addScript('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
$document->addStyleSheet(JUri::root(true).'components/com_alkasubscriptions/assets/css/style.css');
$document->addScriptDeclaration('
    var availableRenewals = '.json_encode($availableRenewals).';
    jQuery(document).ready(function(){
        '.(!$showYellowBlock?'jQuery("#yellowBlock").hide();jQuery("#automatch").show();':'jQuery("#automatch").hide();').'
        jQuery("input[type=\'submit\'].hikashop_cart_input_button").unbind("onclick").attr("onclick","").on("click",function(){
            console.log("click");
            if(jQuery("[name^=\'hikashop_alkasubscriptions_renew\']").length>=1){
                var values = jQuery("[name^=\'hikashop_alkasubscriptions_renew\']").map(function(){return jQuery(this).val();}).get();
                values = values.join("");
                if(values!=""){
                    normalHikashopSubmit(jQuery(this));
                }else{
                    swal({
                        title: "Commandez-vous bien un nouvel abonnement ?",
                        //text: "Nous avons constaté que vous n\'avez pas choisi d\'abonnement à prolonger. Nous allons donc considérer que cette commande concerne un nouvel abonnement (et pas une prolongation).",
                        icon: "warning",

                        dangerMode: true,
                        buttons: {
                            confirm: {
                                text: "Oui (top !)",
                                value: true,
                                visible: true,
                                className: "ttata",
                                closeModal: true
                            },
                            cancel: {
                                text: "Non, je retourne alors au cadre jaune pour prolonger un abonnement existant",
                                value: null,
                                visible: true,
                                className: "",
                                closeModal: true,
                              }

                        }
                    })
                    .then((next) => {
                        if (next) {
                            normalHikashopSubmit(jQuery(this))
                        }
                    });

                }
            }
            return false;
        });

    });

    function normalHikashopSubmit(btn){
        var field=document.getElementById(\'hikashop_product_quantity_field_2\');if(hikashopCheckChangeForm(\'order\',\'hikashop_checkout_form\')){ if(hikashopCheckMethods()){ document.getElementById(\'hikashop_validate\').value=1; btn.disabled = true; document.forms[\'hikashop_checkout_form\'].submit();}} return false;
    }

    function preLockAlkaRenewal(){
        var fields = jQuery(".alkasubscriptions-renews");
        jQuery.each(fields,function(i,e){
            lockAlkaRenewal(jQuery(e), null);
        });

    }
    let lockAlkaRenewalCorresp = [];
    function lockAlkaRenewal(field, subType){
        var fieldId = field.attr("id");
        var previousValue = lockAlkaRenewalCorresp[fieldId];
        var similarFields = jQuery(".alkasubscriptions-renews").not(field);
        var fieldValue = field.val();
        var selectedOption = field.find(":selected")[0];
        var productInCartId = jQuery(selectedOption).attr("data-product-in-cart-id");

        console.log(selectedOption);
        jQuery.each(similarFields,function(i,e){
            if(previousValue){
                jQuery(e).find("option[data-product-in-cart-id=\'"+previousValue+"\']").prop("disabled", false);
            }

            if(productInCartId){
                jQuery(e).find("option[data-product-in-cart-id=\'"+productInCartId+"\']").prop("disabled", true);
            }
            jQuery(e).chosen("destroy").chosen();
        });

        lockAlkaRenewalCorresp[fieldId] = productInCartId;

        /*var available = availableRenewals[subType];
        var countAllocated = 0;
        var similarFields = jQuery(".alkasubscriptions-renews");
        jQuery.each(similarFields,function(i,e){
            if(jQuery(e).val()!=""){
                countAllocated++;
            }
            jQuery(e).prop("disabled",false).chosen("destroy").chosen();
        });

        if(countAllocated >= available){
            jQuery.each(jQuery(".alkasubscriptions-renews[data-subscription-type=\'"+subType+"\']").not(field),function(ind,ele){
                if(jQuery(ele).val()==""){
                    jQuery(ele).prop("disabled",true).chosen("destroy").chosen()
                }
            });
        }*/
    }
');
