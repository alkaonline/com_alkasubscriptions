<?php
defined('_JEXEC') or die('Restricted access');
class plgHikashopAlkasubscriptions extends JPlugin
{
    function __construct(&$subject, $config){
        parent::__construct($subject, $config);
    }

    function onBeforeMailPrepare(&$mail,&$mailer,&$do){
        $mailer->addRecipient('copieboutique@foretnature.be');
    }


    function prepareTableProduct(){
        $db    = JFactory::getDBO();
        $db->setQuery("SHOW COLUMNS FROM `#__hikashop_product` LIKE 'product_alkasubscription_name'");
        $exist = $db->loadObjectList();
        if(!$exist){
            $db->setQuery("ALTER TABLE `#__hikashop_product` ADD `product_alkasubscription_name` VARCHAR(255) NULL DEFAULT NULL");
            $db->query();

            $db    = JFactory::getDBO();
            $db->setQuery("SHOW COLUMNS FROM `#__hikashop_product` LIKE 'product_alkasubscription'");
            $exist = $db->loadObjectList();
            if(!$exist){
                $db->setQuery("ALTER TABLE `#__hikashop_product` ADD `product_alkasubscription` INT NOT NULL DEFAULT 0");
                $db->query();

                $db->setQuery("SHOW COLUMNS FROM `#__hikashop_product` LIKE 'product_alkasubscription_duration'");
                $exist = $db->loadObjectList();
                if(!$exist){
                    $db->setQuery("ALTER TABLE `#__hikashop_product` ADD `product_alkasubscription_duration` INT NULL DEFAULT NULL");
                    $db->query();

                    $db->setQuery("SHOW COLUMNS FROM `#__hikashop_product` LIKE 'product_alkasubscription_groups_in_on_leave'");
                    $exist = $db->loadObjectList();
                    if(!$exist){
                        $db->setQuery("ALTER TABLE `#__hikashop_product` ADD `product_alkasubscription_groups_in_on_leave` TEXT NULL DEFAULT NULL");
                        $db->query();

                        $db->setQuery("SHOW COLUMNS FROM `#__hikashop_product` LIKE 'product_alkasubscription_groups_out_on_leave'");
                        $exist = $db->loadObjectList();
                        if(!$exist){
                            $db->setQuery("ALTER TABLE `#__hikashop_product` ADD `product_alkasubscription_groups_out_on_leave` TEXT NULL DEFAULT NULL");
                            $db->query();

                            $db->setQuery("SHOW COLUMNS FROM `#__hikashop_product` LIKE 'product_alkasubscription_groups_in_on_enter'");
                            $exist = $db->loadObjectList();
                            if(!$exist){
                                $db->setQuery("ALTER TABLE `#__hikashop_product` ADD `product_alkasubscription_groups_in_on_enter` TEXT NULL DEFAULT NULL");
                                $db->query();

                                $db->setQuery("SHOW COLUMNS FROM `#__hikashop_product` LIKE 'product_alkasubscription_groups_out_on_enter'");
                                $exist = $db->loadObjectList();
                                if(!$exist){
                                    $db->setQuery("ALTER TABLE `#__hikashop_product` ADD `product_alkasubscription_groups_out_on_enter` TEXT NULL DEFAULT NULL");
                                    $db->query();
                                                        // $db->setQuery("SHOW COLUMNS FROM `#__hikashop_product` LIKE 'product_alkasubscription_product_to_renew'");
                                                        // $exist = $db->loadObjectList();
                                                        // if(!$exist){
                                                        //                     $db->setQuery("ALTER TABLE `#__hikashop_product` ADD `product_alkasubscription_product_to_renew` INT NULL DEFAULT NULL");
                                                        //         $db->query();
                                                        //             }
                                }
                            }
                        }
                    }
                }
            }
        }


        $db->setQuery("SHOW COLUMNS FROM `#__hikashop_product` LIKE 'product_alkasubscription_issue_quantity'");
        $exist = $db->loadObjectList();
        if(!$exist){
            $db->setQuery("ALTER TABLE `#__hikashop_product` ADD `product_alkasubscription_issue_quantity` INT NULL DEFAULT NULL");
            $db->query();
        }
        $db->setQuery("SHOW COLUMNS FROM `#__hikashop_product` LIKE 'product_alkasubscription_type'");
        $exist = $db->loadObjectList();
        if(!$exist){
            $db->setQuery("ALTER TABLE `#__hikashop_product` ADD `product_alkasubscription_type` VARCHAR(3) NULL DEFAULT NULL");
            $db->query();
        }
        $db->setQuery("SHOW COLUMNS FROM `#__hikashop_product` LIKE 'product_alkasubscription_can_be_renew_with'");
        $exist = $db->loadObjectList();
        if(!$exist){
            $db->setQuery("ALTER TABLE `#__hikashop_product` ADD `product_alkasubscription_can_be_renew_with` TEXT NULL DEFAULT NULL");
            $db->query();
        }
        $db->setQuery("SHOW COLUMNS FROM `#__hikashop_product` LIKE 'product_alkasubscription_can_be_renew_with_main'");
        $exist = $db->loadObjectList();
        if(!$exist){
            $db->setQuery("ALTER TABLE `#__hikashop_product` ADD `product_alkasubscription_can_be_renew_with_main` INT NULL DEFAULT NULL");
            $db->query();
        }
    }

    /*function onBeforeProductUpdate(& $element, & $do){
        return $this->onBeforeProductCreate($element, $do);
    }*/

    function onBeforeOrderCreate(&$order,&$send_email) {
        $db = JFactory::getDbo();
        $db->setQuery("SHOW COLUMNS FROM `#__hikashop_order` LIKE 'order_alkasubscription_resubscription_options'");
        $exist = $db->loadObjectList();
        if(!$exist){
            $db->setQuery("ALTER TABLE `#__hikashop_order` ADD `order_alkasubscription_resubscription_options` text NULL DEFAULT NULL");
            $db->query();
        }
        $order->order_alkasubscription_resubscription_options = json_encode(hikaInput::get()->get('hikashop_alkasubscriptions_renew',array(),'array'));

    }

    function onAfterOrderCreate( &$order,&$send_email){
        return $this->onAfterOrderUpdate( $order,$send_email);
    }

    function onAfterOrderUpdate( &$order,&$send_email){

        $params     = JComponentHelper::getParams('com_alkasubscriptions');
        $lang = JFactory::getLanguage();
        $lang->load('com_alkasubscriptions', JPATH_SITE, $lang->getTag(), true);

        $config =& hikashop_config();
        $confirmed = explode(',',$config->get('order_confirmed_status'));
        $created = explode(',',$config->get('order_created_status'));
        $cancelled = explode(',',$config->get('cancelled_order_status'));

        if(!empty($order->order_type) && $order->order_type != 'sale')
            return true;

        require_once (JPATH_SITE . '/components/com_alkasubscriptions/helpers/alkasubscriptions.php');
        if(in_array($order->order_status, $created)){

            $mainframe = JFactory::getApplication();
            $db = JFactory::getDBO();

            $class = hikashop_get('class.order');
            $dbOrder = $class->get($order->order_id);
            $fullOrder = $class->loadFullOrder($order->order_id, false, false);

            $addressClass=hikashop_get('class.address');
            $orderAddress = $addressClass->get($dbOrder->order_shipping_address_id);

            $class = hikashop_get('class.user');
            $data = $class->get($dbOrder->order_user_id);

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select(array('#__alkasubscriptions_subscription.*'));
            $query->from($db->quoteName('#__alkasubscriptions_subscription_order'));
            $query->join('left',$db->quoteName('#__alkasubscriptions_subscription').' ON '.$db->quoteName('#__alkasubscriptions_subscription_order.id_subscription').'='.$db->quoteName('#__alkasubscriptions_subscription.id'));
            $query->where($db->quoteName('#__alkasubscriptions_subscription_order.id_order') . ' = '. $db->quote($order->order_id));
            $db->setQuery($query);
            $existingSubs = $db->loadObjectList();
            $existingSubsController = array();
            foreach($existingSubs as $existingSub){
                    $existingSubsController[$existingSub->idProduct][$existingSub->productOptions] = intval($existingSubsController[$existingSub->idProduct][$existingSub->productOptions])+1;
            }



            $productClass = hikashop_get('class.product');
            $history = '';
            //////***************************************************************************************//////
            $productUsedToRenew = array();
            $order->order_alkasubscription_resubscription_options = json_decode($order->order_alkasubscription_resubscription_options) ? json_decode($order->order_alkasubscription_resubscription_options) : array();
            if(sizeof($order->order_alkasubscription_resubscription_options)){
                foreach($order->order_alkasubscription_resubscription_options as $subscriptionId=>$productId){
                    $productUsedToRenew[$productId] += 1;
                    //Extends subscription $subscriptionId with $productId if status = update
                }
            }

            //if ((isset($productUsedToRenew[$productId]) && $result->order_product_quantity > $productUsedToRenew[$productId]) || !isset($productUsedToRenew[$productId])){
                // $howMuchToCreate = isset($productUsedToRenew[$productId]) ? $result->order_product_quantity-$productUsedToRenew[$productId] : $result->order_product_quantity;
                //for($i=1;$i<=$howMuchToCreate;$i++){
                    //Process habituel de creation (a verifier)
                //}
            //}

            //////***************************************************************************************//////
            //For each products
            foreach($fullOrder->products as $result){

                $product_id = $result->product_id;
                //subscription_level_id from the product
                $product = $productClass->get($product_id);

                if(is_object($product)){
                    if($product->product_type == 'variant' && $product->product_alkasubscription == '0'){
                        $productClass = hikashop_get('class.product');
                        $product = $productClass->get($product->product_parent_id);
                    }
                    $product_alkasubscription = $product->product_alkasubscription;
                }else{
                    $product_alkasubscription = 0;
                }



                if($product_alkasubscription!=0){



                    if ((isset($productUsedToRenew[$product_id]) && $result->order_product_quantity > $productUsedToRenew[$product_id]) || !isset($productUsedToRenew[$product_id])){
                        JFactory::getApplication()->enqueueMessage(JText::_('COM_ALKASUBCRIPTIONS_ORDER_SUCCESS').'<a href="'.JRoute::_('index.php?option=com_alkasubscriptions&view=subscriptions&Itemid='.JFactory::getApplication()->input->get('Itemid',null,null)).'"><br/>'.JText::_('COM_ALKASUBCRIPTIONS_ORDER_SUCCESS_LINK').'</a>.', 'Warning');
                        $howMuchToCreate = isset($productUsedToRenew[$product_id]) ? $result->order_product_quantity-$productUsedToRenew[$product_id] : $result->order_product_quantity;
                        for($i=1;$i<=$howMuchToCreate;$i++){
                            //check address presence
                            $db = JFactory::getDbo();
                            $query = $db->getQuery(true);
                            $query->select($db->quoteName(array('id')));
                            $query->from($db->quoteName('#__alkasubscriptions_address'));
                            $query->where($db->quoteName('hikaAddressId') . ' = '. $db->quote($orderAddress->address_id));
                            $query->where($db->quoteName('idUser') . ' = '. $db->quote($data->user_cms_id));
                            $query->setLimit('1');
                            $db->setQuery($query);

                            $alkaAddressId = $db->loadResult();

                            $db = JFactory::getDbo();
                            $query = $db->getQuery(true);
                            $query->select($db->quoteName(array('zone_name')));
                            $query->from($db->quoteName('#__hikashop_zone'));
                            $query->where($db->quoteName('zone_namekey') . ' = '. $db->quote($orderAddress->address_country));
                            $query->setLimit('1');
                            $db->setQuery($query);

                            $humanCountry = $db->loadResult();

                            $now = new DateTime();
                            if(!$alkaAddressId){
                                $nAddress = new StdClass();
                                $nAddress->title            = $orderAddress->address_title;
                                $nAddress->firstname        = $orderAddress->address_firstname;
                                $nAddress->lastname         = $orderAddress->address_lastname;
                                $nAddress->company          = $orderAddress->address_company;
                                $nAddress->addressLine1     = $orderAddress->address_street;
                                $nAddress->addressLine2     = $orderAddress->address_stree2;
                                $nAddress->city             = $orderAddress->address_city;
                                $nAddress->zip              = $orderAddress->address_post_code;
                                $nAddress->country          = $humanCountry;
                                $nAddress->createdDate      = $now->format('Y-m-d H:i:s');
                                $nAddress->createdBy        = $data->user_cms_id;
                                $nAddress->enabled          = 1 ;
                                $nAddress->note             = '';
                                $nAddress->hikaAddressId    = $orderAddress->address_id;
                                $nAddress->idUser           = $data->user_cms_id;
                                $db = JFactory::getDbo();
                                $db->insertObject('#__alkasubscriptions_address', $nAddress);
                                $alkaAddressId = $db->insertid();
                            }
                            if($params->get('mode') == 'duration'){
                                $endDate = ($product->product_alkasubscription_duration==-1)?null:$now->add(new DateInterval('P'.$product->product_alkasubscription_duration.'D'));
                            }else{
                                $endDate = null;
                            }


                            $deadlines = AlkaSubscriptionsHelper::getDeadlines($product->product_alkasubscription_type, $product->product_alkasubscription_issue_quantity);

                            $now = new DateTime();
                            $id         = null;
                            $name       = $product->product_alkasubscription_name;
                            $startDate  = $now->format("Y-m-d H:i:s");
                            $endDate    = ($product->product_alkasubscription_duration==-1||$endDate==null)?'NULL':$endDate->format("Y-m-d H:i:s");
                            $renewDate  = $now->format("Y-m-d H:i:s");
                            $forever    = ($product->product_alkasubscription_duration==-1)?1:0;
                            $enabled    = 1;
                            $idUser     = $data->user_cms_id;
                            $idProduct  = $product->product_id;
                            $idOrder    = $order->order_id;
                            $idAddress  = $alkaAddressId;
                            if($existingSubsController[$idProduct][serialize($result->order_product_options)]>0){
                                $existingSubsController[$idProduct][serialize($result->order_product_options)] = intval($existingSubsController[$idProduct][serialize($result->order_product_options)])-1;
                            }else{
                                if(in_array($fullOrder->order_status, $confirmed)){
                                    $paid=1;
                                }else{
                                    $paid=0;
                                }
                                $insert = AlkaSubscriptionsHelper::setSubscription($id, $name, $startDate, $endDate, $renewDate, $forever, $enabled, $idUser, $idProduct, $idOrder, $idAddress, $result->order_product_options,$product->product_alkasubscription_type, $deadlines[0]->issue,end($deadlines)->issue,$paid);
                            }

                            if($insert){
                                $history .= 'AlkaSubscription #'.$insert.' created<br/>';
                            }
                        }
                    }
                }
            }

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);

            $query->select($db->quoteName(array('history_id')));
            $query->from($db->quoteName('#__hikashop_history'));
            $query->where($db->quoteName('history_order_id') . ' = '. $db->quote($order->order_id));
            $query->order('history_id DESC');
            $query->setLimit('1');
            $db->setQuery($query);

            $historyId = $db->loadResult();
            $Nhistory = new StdClass();
            $Nhistory->history_id = $historyId;
            $Nhistory->history_data = $history;
            JFactory::getDbo()->updateObject('#__hikashop_history', $Nhistory, 'history_id');
        }else if(in_array($order->order_status, $confirmed)){
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);

            $query->select("#__alkasubscriptions_subscription.*");
            $query->from($db->quoteName('#__alkasubscriptions_subscription_order'));
            $query->join('INNER',$db->quoteName('#__alkasubscriptions_subscription') . ' ON ('.$db->quoteName("#__alkasubscriptions_subscription.id").' = '.$db->quoteName("#__alkasubscriptions_subscription_order.id_subscription").') ');
            $query->join('LEFT',$db->quoteName('#__hikashop_product') . ' ON ('.$db->quoteName("#__hikashop_product.product_id").' = '.$db->quoteName("#__alkasubscriptions_subscription.idProduct").') ');
            $query->where($db->quoteName('#__alkasubscriptions_subscription_order.id_order') . ' = '. $db->quote($order->order_id));
            $db->setQuery($query);
            $subs = $db->loadObjectList();
            $now = new DateTime();
            $productClass = hikashop_get('class.product');

            if(sizeof($subs)>0){
                foreach($subs as $sub){
                    $product = $productClass->get($sub->idProduct);
                    $deadlines = AlkaSubscriptionsHelper::getDeadlines($sub->subscriptionType,$product->product_alkasubscription_issue_quantity);

                    if($params->get('mode') == 'duration'){
                        $endDate            = ($sub->product_alkasubscription_duration==-1)?null:$now->add(new DateInterval('P'.$sub->product_alkasubscription_duration.'D'));
                    }else{
                        $endDate = null;
                    }
                    $now                = new DateTime();
                    $nSub               = new StdClass();
                    $nSub->id           = $sub->id;
                    $nSub->startDate    = $now->format('Y-m-d H:i:s');
                    $nSub->endDate      = $endDate!=null?$endDate->format('Y-m-d H:i:s'):null;
                    $nSub->paid         = 1;
                    $nSub->enabled      = 1;
                    $nSub->modifiedBy   = JFactory::getUser()->id;
                    $nSub->modifiedDate = $now->format('Y-m-d H:i:s');
                    $nSub->startIssueNumber = $deadlines[0]->issue;
                    $nSub->endIssueNumber = end($deadlines)->issue;

                    $renewHistory = new StdClass();
                    $renewHistory->subscription_id = $sub->id;
                    $renewHistory->start = $deadlines[0]->date;
                    $renewHistory->code = $product->product_code;
                    $renewHistory->productId = $product->product_id;
                    $renewHistory->startIssue = $nSub->startIssueNumber;
                    $renewHistory->end = end($deadlines)->date;
                    $renewHistory->endIssue = $nSub->endIssueNumber;
                    $renewHistory->productName = $product->product_alkasubscription_name;
                    $renewHistory->productType = $product->product_alkasubscription_type;
                    $renewHistory->orderId = $order->order_id;

                    JFactory::getDbo()->insertObject('#__alkasubscriptions_renew_history', $renewHistory);
                    JFactory::getDbo()->updateObject('#__alkasubscriptions_subscription', $nSub, 'id');
                }
                AlkaSubscriptionsHelper::setGroups(null,$order->order_id);
            }

            $orderClass = hikashop_get('class.order');
            $orderFull = $orderClass->loadFullOrder($order->order_id, true, false);
            $orderFull->order_alkasubscription_resubscription_options = @json_decode($orderFull->order_alkasubscription_resubscription_options);

            $now = new DateTime();
            if(sizeof($orderFull->order_alkasubscription_resubscription_options)){
                $productClass = hikashop_get('class.product');
                foreach($orderFull->order_alkasubscription_resubscription_options as $subscriptionId=>$productId){
                    $db = JFactory::getDbo();
                    $query = $db->getQuery(true);

                    $query->select("#__alkasubscriptions_subscription.*");
                    $query->from($db->quoteName('#__alkasubscriptions_subscription'));

                    $query->where($db->quoteName('#__alkasubscriptions_subscription.id') . ' = '. $db->quote($subscriptionId));
                    $db->setQuery($query);
                    $subscription = $db->loadObject();

                    $product = $productClass->get($productId);
                    $subscription->ordersRenews = (array)json_decode($subscription->ordersRenews);
                    if(!in_array((int)$orderFull->order_id, $subscription->ordersRenews) && $productId && $product->product_id!=='' && $product->product_id!==null){
                        $update = new stdClass();
              					$update->order_id = $orderFull->order_id;
              					$update->history = new stdClass();
              					$update->history->history_reason = 'Subscription '.$subscription->id.' extended of '.$product->product_alkasubscription_issue_quantity.' issues. Previous range : '.$subscription->startIssueNumber.'->'.$subscription->endIssueNumber.' New range : '.$subscription->startIssueNumber.'->'.($subscription->endIssueNumber+$product->product_alkasubscription_issue_quantity);
              					$update->history->history_notified = 0;
              					$orderClass->save($update);

                        $deadlines = AlkaSubscriptionsHelper::getDeadlines($subscription->subscriptionType,$product->product_alkasubscription_issue_quantity);

                        $subscription->endIssueNumber = $subscription->endIssueNumber+$product->product_alkasubscription_issue_quantity;
                        $subscription->renewDate =$now->format('Y-m-d');
                        $subscription->ordersRenews[] = (int)$orderFull->order_id;
                        $subscription->ordersRenews = json_encode($subscription->ordersRenews);

                        $renewHistory = new StdClass();
                        $renewHistory->subscription_id = $subscription->id;
                        $renewHistory->code = $product->product_code;
                        $renewHistory->productId = $product->product_id;
                        $renewHistory->startIssue = $subscription->endIssueNumber-($product->product_alkasubscription_issue_quantity-1);
                        $renewHistory->start = AlkaSubscriptionsHelper::getDeadlineDateByIssue($renewHistory->startIssue,$product->product_alkasubscription_type);
                        $renewHistory->endIssue = $subscription->endIssueNumber;
                        $renewHistory->end = AlkaSubscriptionsHelper::getDeadlineDateByIssue($renewHistory->endIssue,$product->product_alkasubscription_type);
                        $renewHistory->productName = $product->product_alkasubscription_name;
                        $renewHistory->productType = $product->product_alkasubscription_type;
                        $renewHistory->orderId = $order->order_id;

                        JFactory::getDbo()->insertObject('#__alkasubscriptions_renew_history', $renewHistory);

                        JFactory::getDbo()->updateObject('#__alkasubscriptions_subscription', $subscription, 'id');
                    }
                }
            }


        }else if(in_array($order->order_status, $cancelled) && $params->get('mode') == 'duration'){
            /*$now = new DateTime();
            $Subscription = new StdClass();
            $Subscription->idOrder = $order->order_id;
            $Subscription->enabled = 0;
            $Subscription->modifiedBy = JFactory::getUser()->id;
            $Subscription->modifiedDate = $now->format('Y-m-d H:i:s');
            JFactory::getDbo()->updateObject('#__alkasubscriptions_subscription', $Subscription, 'idOrder');
            AlkaSubscriptionsHelper::setGroups(null,$order->order_id);*/
        }


        return true;
    }

    function onProductFormDisplay(& $product, & $html){
        $this->prepareTableProduct();
        // Get plugin 'my_plugin' of plugin type 'my_plugin_type'
        $plugin = JPluginHelper::getPlugin('hikashop', 'alkasubscriptions');
        $pluginParams = new JRegistry($plugin->params);
        $params     = JComponentHelper::getParams('com_alkasubscriptions');

        //$pluginParams->get('param1');

        $pType = 'product';
        if($product->product_type == 'variant')
            $pType = 'variant';

        //Souscription field
        $content = '<dl class="hika_options"><dt class="key">'.JText::_( 'Subscription' ).'</dt><dd><select  name="data['.$pType.'][product_alkasubscription]">';
            $content .="<option value='0'>".JText::_('JNO')."</option>";
            $content .="<option value='1' ".($product->product_alkasubscription?'selected="selected"':'').">".JText::_('JYES')."</option>";
        $content .= '</select></dd></dl>';
        $html[] = $content;

        //Name
        $content = '<dl class="hika_options"><dt class="key">'.JText::_( 'Subscription name' ).'</dt><dd><input type="text"  name="data['.$pType.'][product_alkasubscription_name]" value="'.($product->product_alkasubscription_name?$product->product_alkasubscription_name:$product->product_name).'"/></dd></dl>';
        $html[] = $content;

        //Duration
        if($params->get('mode') == 'duration'){
            $content = '<dl class="hika_options"><dt class="key">'.JText::_( 'Duration (in days) (-1 for subscription with no end)' ).'</dt><dd><input type="number" min="-1"  name="data['.$pType.'][product_alkasubscription_duration]" value="'.$product->product_alkasubscription_duration.'"/></dd></dl>';
        }else if($params->get('mode') == 'issue'){
            $content = '<dl class="hika_options hidden"><dt class="key">'.JText::_( 'Duration (in days) (-1 for subscription with no end)' ).'</dt><dd><input type="number" min="-1"  name="data['.$pType.'][product_alkasubscription_duration]" value="'.$product->product_alkasubscription_duration.'"/></dd></dl>';
        }
        $html[] = $content;

        //Issue number
        if($params->get('mode') == 'issue'){
            $content = '<dl class="hika_options"><dt class="key">'.JText::_( 'Number of issues' ).'</dt><dd><input type="number" min="1"  name="data['.$pType.'][product_alkasubscription_issue_quantity]" value="'.$product->product_alkasubscription_issue_quantity.'"/></dd></dl>';
        }else if($params->get('mode') == 'duration'){
            $content = '<dl class="hika_options hidden"><dt class="key">'.JText::_( 'Number of issues' ).'</dt><dd><input type="number" min="-1"  name="data['.$pType.'][product_alkasubscription_issue_quantity]" value="'.$product->product_alkasubscription_issue_quantity.'"/></dd></dl>';
        }
        $html[] = $content;

        //Subscrition code
        if($params->get('mode') == 'issue'){
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);

            $query->select("DISTINCT ".$db->quoteName('subscription'));
            $query->from($db->quoteName('#__alkasubscriptions_deadline'));
            $db->setQuery($query);
            $deadLinesTypes = $db->loadColumn();

            $content = '<dl class="hika_options"><dt class="key">'.JText::_( 'Deadlines' ).'</dt><dd><select id="product_alkasubscription_type" name="data['.$pType.'][product_alkasubscription_type]">';
                foreach($deadLinesTypes as $deadLinesType){
                    $content .="<option value='".$deadLinesType."' ".($product->product_alkasubscription_type == $deadLinesType ? 'selected="selected"':'').">".$deadLinesType."</option>";
                }
            $content .= '</select></dd></dl>';
        }else if($params->get('mode') == 'duration'){
            $content = '<dl class="hika_options hidden"><dt class="key">'.JText::_( 'Subscription code (FO, SAL, etc.)' ).'</dt><dd><input type="text"  maxlength="3"  name="data['.$pType.'][product_alkasubscription_type]" value="'.$product->product_alkasubscription_type.'"/></dd></dl>';
        }
        $html[] = $content;

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select("*");
        $query->from($db->quoteName('#__usergroups'));
        $db->setQuery($query);

        $groups = $db->loadObjectList();

        $content = '<dl class="hika_options"><dt class="key">'.JText::_( 'Groups to join on subscription start' ).'</dt><dd><select  multiple="multiple" name="data['.$pType.'][product_alkasubscription_groups_in_on_enter][]">';
                $selectedGroups = explode(',',$product->product_alkasubscription_groups_in_on_enter);
                foreach($groups as $group){
                    $content .="<option value='".$group->id."' ".((in_array($group->id,$selectedGroups))?"selected='selected'":"").">".$group->title."</option>";
                }
        $content .= '</select></dd></dl>';
        $html[] = $content;

        $content = '<dl class="hika_options"><dt class="key">'.JText::_( 'Groups to leave on subscription start' ).'</dt><dd><select  multiple="multiple" name="data['.$pType.'][product_alkasubscription_groups_out_on_enter][]">';
                $selectedGroups = explode(',',$product->product_alkasubscription_groups_out_on_enter);
                foreach($groups as $group){
                    $content .="<option value='".$group->id."' ".((in_array($group->id,$selectedGroups))?"selected='selected'":"").">".$group->title."</option>";
                }
        $content .= '</select></dd></dl>';
        $html[] = $content;

        $content = '<dl class="hika_options"><dt class="key">'.JText::_( 'Groups to join on subscription end' ).'</dt><dd><select  multiple="multiple" name="data['.$pType.'][product_alkasubscription_groups_in_on_leave][]">';
                $selectedGroups = explode(',',$product->product_alkasubscription_groups_in_on_leave);
                foreach($groups as $group){
                    $content .="<option value='".$group->id."' ".((in_array($group->id,$selectedGroups))?"selected='selected'":"").">".$group->title."</option>";
                }
        $content .= '</select></dd></dl>';
        $html[] = $content;

        $content = '<dl class="hika_options"><dt class="key">'.JText::_( 'Groups to leave on subscription end' ).'</dt><dd><select  multiple="multiple" name="data['.$pType.'][product_alkasubscription_groups_out_on_leave][]">';
                $selectedGroups = explode(',',$product->product_alkasubscription_groups_out_on_leave);
                foreach($groups as $group){
                    $content .="<option value='".$group->id."' ".((in_array($group->id,$selectedGroups))?"selected='selected'":"").">".$group->title."</option>";
                }
        $content .= '</select></dd></dl>';
        $html[] = $content;
        // $content = '<dl class="hika_options"><dt class="key">'.JText::_( 'Product for renew' ).'</dt><dd><select id="subsciption_levels" name="data['.$pType.'][product_alkasubscription_product_to_renew]" >';
        // $db = JFactory::getDbo();
        // $query = $db->getQuery(true);
        //
        // $query->select("*");
        // $query->from($db->quoteName('#__hikashop_product'));
        // $query->where($db->quoteName('#__hikashop_product.product_alkasubscription')."=1");
        // $query->where($db->quoteName('#__hikashop_product.product_published')."=1");
        // $db->setQuery($query);
        // $productsWithSubs = $db->loadObjectList();
        //     $content.="<option value''> - </option>";
        //     foreach($productsWithSubs as $productWithSubs){
        //         $content .="<option value='".$productWithSubs->product_id."' ".(($productWithSubs->product_id == $product->product_alkasubscription_product_to_renew)?"selected='selected'":"").">".$productWithSubs->product_name."</option>";
        //     }
        //     $content .= '</select></dd></dl>';
        // $html[] = $content;
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select("*");
        $query->from($db->quoteName('#__hikashop_product'));
        //$query->where('(product_alkasubscription_can_be_renew_with IS NULL OR product_alkasubscription_can_be_renew_with="")');
        $query->where('product_alkasubscription = 1');
        $db->setQuery($query);

        $productToRenew = $db->loadObjectList();
        $selectedProducts = explode(',',trim($product->product_alkasubscription_can_be_renew_with));
        $content = '<dl class="hika_options"><dt class="key">'.JText::_( 'Can be renew with' ).'</dt><dd><select onchange="javascript:set_subsciption_levels()" id="subsciption_levels" multiple="multiple" name="data['.$pType.'][product_alkasubscription_can_be_renew_with][]">';
                foreach($productToRenew as $prod){
                    $content .="<option value='".$prod->product_id."' ".((in_array($prod->product_id,$selectedProducts))?"selected='selected'":"").">".$prod->product_name." (".$prod->product_code.")</option>";
                }
        $content .= '</select></dd></dl>';
        $content .= '<dl class="hika_options"><dt class="key">'.JText::_( 'Default product to renew' ).'</dt><dd><select id="product_alkasubscription_can_be_renew_with_main" name="data['.$pType.'][product_alkasubscription_can_be_renew_with_main]">';
                $content.="<option id='empty_default'> - ".JText::_('None')." - </option>";
                foreach($productToRenew as $prod){
                    $content .="<option data-id='".$prod->product_id."' value='".$prod->product_id."' ".(($prod->product_id == $product->product_alkasubscription_can_be_renew_with_main)?"selected='selected'":"").">".$prod->product_name." (".$prod->product_code.")</option>";
                }
        $content .= '</select></dd></dl>';
        $html[] = $content;
        $document = JFactory::getDocument();
        $document->addScriptDeclaration('
            jQuery(document).ready(function(){
              set_subsciption_levels();
            });
            function set_subsciption_levels(){
              jQuery("#product_alkasubscription_can_be_renew_with_main option").not("#empty_default").each(function(i,e){
                var thisId = jQuery(e).data("id");
                console.log(jQuery(\'#subsciption_levels option[value="\'+thisId+\'"]\'));
                if(jQuery(\'#subsciption_levels option[value="\'+thisId+\'"]\').prop("selected")){
                  jQuery(e).prop("disabled",false);
                }else{
                  jQuery(e).prop("selected", false);
                  jQuery(e).prop("disabled",true);
                }
              });
              jQuery(\'#product_alkasubscription_can_be_renew_with_main\').chosen("destroy").chosen();

            }
        ');
    }

    public function onCheckoutStepList(&$list) {
        //add step on checkout workflow
        $list['plg.shop.renewsubscriptions'] = JText::_("Choisir abonnement(s) à renouveler");
    }


    public function onCheckoutStepDisplay($layoutName, &$html, &$view, $pos=null, $options=null){
      $db = JFactory::getDbo();
      $query = $db->getQuery(true);
      $query->select("#__alkasubscriptions_subscription.*, #__alkasubscriptions_address.*");
      $query->from($db->quoteName('#__alkasubscriptions_subscription_order'));
      $query->join('LEFT', $db->quoteName('#__alkasubscriptions_subscription').' ON ('.$db->quoteName('#__alkasubscriptions_subscription.id').' = '.$db->quoteName('#__alkasubscriptions_subscription_order.id_subscription').')');
      $query->join('LEFT', $db->quoteName('#__alkasubscriptions_address').' ON ('.$db->quoteName('#__alkasubscriptions_address.id').' = '.$db->quoteName('#__alkasubscriptions_subscription.idAddress').')');
      $query->where($db->quoteName('#__alkasubscriptions_subscription_order.id_order') . ' = '. $db->quote(5669));
      $db->setQuery($query);
$subscriptions = $db->loadObjectList();

        if($layoutName=='plg.shop.renewsubscriptions'){
            if(!@include_once(rtrim(JPATH_ADMINISTRATOR,DS).DS.'components'.DS.'com_hikashop'.DS.'helpers'.DS.'helper.php')){ return false; }
            require_once (JPATH_SITE . '/components/com_alkasubscriptions/helpers/alkasubscriptions.php');

            $cartClass = hikashop_get('class.cart');
            $cart = $cartClass->loadFullCart();
            $products = $cart->products;

            // SELECT SUBSCRIPTION OF ACTUAL USER
            $user = JFactory::getUser();
            $activeSubscriptionsQuery = AlkaSubscriptionsHelper::getActiveSubscriptionQuery();
            $db     = JFactory::getDBO();

            //ADD address info
            $activeSubscriptionsQuery->select(
                $db->quoteName(array(
                        'a.title',
                        'a.company',
                        'a.firstname',
                        'a.lastname',
                        'a.addressLine1',
                        'a.addressLine2',
                        'a.zip',
                        'a.city',
                        'a.country',
                        'p.product_name',
                        'p.product_alkasubscription_can_be_renew_with'
                ))
            );

            $activeSubscriptionsQuery->select($db->quoteName('sd.date', 'startIssueDate'));
            $activeSubscriptionsQuery->select($db->quoteName('ed.date', 'endIssueDate'));
            $activeSubscriptionsQuery->join('LEFT', $db->quoteName('#__alkasubscriptions_address', 'a')
                . ' ON (' .
                $db->quoteName('s.idAddress')
                . ' = ' .
                $db->quoteName('a.id') . ')');
            $activeSubscriptionsQuery->join('LEFT', $db->quoteName('#__alkasubscriptions_deadline', 'sd')
                . ' ON (' .
                $db->quoteName('sd.subscription')
                . ' = ' .
                $db->quoteName('s.subscriptionType') .
                ' AND '.
                $db->quoteName('sd.issue')
                . ' = ' .
                $db->quoteName('s.startIssueNumber')
                .')');

            $activeSubscriptionsQuery->join('LEFT', $db->quoteName('#__alkasubscriptions_deadline', 'ed')
                . ' ON (' .
                $db->quoteName('ed.subscription')
                . ' = ' .
                $db->quoteName('s.subscriptionType') .
                ' AND '.
                $db->quoteName('ed.issue')
                . ' = ' .
                $db->quoteName('s.endIssueNumber')
                .')');
            $activeSubscriptionsQuery->join('LEFT', $db->quoteName('#__hikashop_product', 'p')
                . ' ON (' .
                $db->quoteName('s.idProduct')
                . ' = ' .
                $db->quoteName('p.product_id') . ')');
            $activeSubscriptionsQuery->where($db->quoteName('s.idUser').'='.$user->id);
            $activeSubscriptionsQuery->where($db->quoteName('s.paid').'=1');
            $db->setQuery($activeSubscriptionsQuery);
            $activeSubscriptions = $db->loadObjectList();
            $subscriptionInCart = array();
            $availableRenewals = array();
            foreach($products as $product){
                if($product->product_alkasubscription){
                    $availableRenewals[$product->product_id] = intval($availableRenewals[$product->product_id]+1);
                    $subscriptionInCart[$product->product_id] = $product;
                }
            }
            if(sizeof($subscriptionInCart)==0){
                return false;
            }

            $renew = false;

            foreach($activeSubscriptions as $k=>$activeSubscription){

                $canBeRenewWith = explode(',',trim($activeSubscription->product_alkasubscription_can_be_renew_with));
                $counter = 0 ;
                $activeSubscriptions[$k]->canBeRenewWith = $canBeRenewWith;

                foreach($canBeRenewWith as $canBeRenewWithId){
                    if(isset($subscriptionInCart[$canBeRenewWithId])){
                        $counter = 1;
                        $renew = true;
                    }
                }

                if($counter == 0){
                    unset($activeSubscriptions[$k]);
                }

            }

            if($renew){
                $app = JFactory::getApplication();
                $path = JPATH_THEMES.'/'.$app->getTemplate().'/plg_hikashop_alkasubscriptions/renew_subscriptions_checkout.php';
                if(!file_exists($path)) {

                        $path = JPATH_SITE.'/plugins/hikashop/alkasubscriptions/renew_subscriptions_checkout.php';
                }
                if(!file_exists($path)){
                    return false;
                }
                require($path);
            }

        }
    }

    public function onAfterCheckoutStep($controllerName, &$go_back, $original_go_back, &$controller){
        if($controllerName != 'plg.shop.renewsubscriptions')
            return false;

        /*$cartClass = hikashop_get('class.cart');
        $cart = $cartClass->getFullCart();
        var_dump($cart);
        var_dump(hikaInput::get()->get('hikashop_alkasubscriptions_renew',array(),'array'));
        die();
        $test = hikaInput::get()->getArray('hikashop_alkasubscriptions_renew');
        var_dump($test);
        die();*/
    }

}
