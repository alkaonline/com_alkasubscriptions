<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.6.4
 * @author	hikashop.com
 * @copyright	(C) 2010-2016 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');

class plgHikashopOrder_avoid_reabonnement extends JPlugin
{
	public function onCheckoutStepList(&$list) {
		//add step on checkout workflow
		$list['plg.shop.verifreabo'] = JText::_("AVOID_REABO");
	}
	
	function onBeforeCheckoutStep($controllerName, &$go_back, $original_go_back, &$controller){
		if($controllerName == 'plg.shop.verifreabo'){
			$this->_checkReAbo();
		}
	}
	
	function onAfterCheckoutStep($controllerName, &$go_back, $original_go_back, &$controller){
		if($controllerName == 'plg.shop.verifreabo'){
			if($this->_checkReAbo()){
				$go_back = true;
			}
		}
	}
	
	function _checkReAbo(){
		$return = false;
		if(!defined(DS)){define(DS, '/');}
		if(!@include_once(rtrim(JPATH_ADMINISTRATOR,DS).DS.'components'.DS.'com_hikashop'.DS.'helpers'.DS.'helper.php')){ return false; }
		
		$cartClass = hikashop_get('class.cart');
		$cart = $cartClass->loadFullCart();
		
		if(sizeof($cart->products)){
			foreach($cart->products as $product){
				if($product->product_subscription_id!=0 && (in_array('14', JFactory::getUser()->groups)||!$this->_checkPreviousAbo())){
					JFactory::getApplication()->enqueueMessage('<strong>Vous avez déjà un abonnement actif.</strong> Il vous est impossible de vous réabonner avant l\'échéance. À l\'échéance, vous recevrez un courrier automatique vous proposant de vous réabonner. Pour éventuellement poursuivre vos achats d\'autres articles, veuillez retirer l\'abonnement de votre panier.', 'error');
					$return = true;
				}
			}
		}
		return $return;
	}

	function _checkPreviousAbo(){
		$return = true;
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select(array('COUNT(*)'));
		$query->from($db->quoteName('#__akeebasubs_subscriptions'));
		$query->where($db->quoteName('publish_up') . ' <= now()');
		$query->where($db->quoteName('publish_down') . ' >= now()');
		$query->where($db->quoteName('user_id') . ' = '.JFactory::getUser()->id);
		$query->where($db->quoteName('enabled') . ' = 1');
		$db->setQuery($query);
		$count = (int)$db->loadResult();
		
		if($count>0){
			$return = false;
		}
		
		return $return;
	}

}
