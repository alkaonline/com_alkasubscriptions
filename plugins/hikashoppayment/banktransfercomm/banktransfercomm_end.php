<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.6.4
 * @author	hikashop.com
 * @copyright	(C) 2010-2016 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><div class="hikashop_banktransfer_end" id="hikashop_banktransfer_end">
	<span class="hikashop_banktransfer_end_message" id="hikashop_banktransfer_end_message">
		<?php echo JText::_('ORDER_IS_COMPLETE').'<br/>'.
		JText::sprintf('PLEASE_TRANSFERT_MONEY',$this->amount).'<br/>'.
		str_replace('{bankcommunication}', $this->bankcommunication, $this->information).
		'<p class="alert alert-danger"><i class="icon-warning-sign"></i>&nbsp;N\'oubliez pas d\'indiquer la communication structurée <strong>'.$this->bankcommunication.'</strong> dans votre virement. Sans quoi votre paiement ne sera pas validé.</p>'.
		JText::_('THANK_YOU_FOR_PURCHASE');?>
	</span>
</div>
<?php
if(!empty($this->return_url)) {
	$doc = JFactory::getDocument();
	$doc->addScriptDeclaration("window.hikashop.ready(function(){window.location='".$this->return_url."'});");
}
