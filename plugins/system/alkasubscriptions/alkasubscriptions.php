<?php

defined('_JEXEC') or die;

class PlgSystemAlkasubscriptions extends JPlugin
{
    public function onAfterInitialise(){
        // $doc = JFactory::getDocument();
        // $doc->addStyleSheet(JUri::root(true).'/media/jui/css/icomoon.css');
        //emptyCart
        $input = JFactory::getApplication()->input;
        if($input->get('show_fn_alert') == 1){
          JFactory::getApplication()->enqueueMessage('La demande de paiement a échoué, merci de réessayer ou de choisir un autre mode de paiement.', 'error');
        }

        $emptyCart = JFactory::getApplication()->input->get('emptyCart');
        if($emptyCart=='1'){
          $hikashopHelper = rtrim(JPATH_ADMINISTRATOR,'/').'/components/com_hikashop/helpers/helper.php';
          if(file_exists($hikashopHelper)){
            include_once($hikashopHelper);
            $cartClass = hikashop_get('class.cart');
            $cartClass->resetCart(false);
            $cart=$cartClass->initCart();
          }
        }
        //AUTOLOG
        $params 	= JComponentHelper::getParams('com_alkasubscriptions');
        $publickey = $params->get('publickey');
        $data = JFactory::getApplication()->input->get($publickey, null,'raw');
        if($data){
            JLoader::register('AlkaSubscriptionsHelper',JPATH_SITE . '/components/com_alkasubscriptions/helpers/alkasubscriptions.php');
            JPluginHelper::importPlugin('user');
            $data = AlkaSubscriptionsHelper::decryptData($data);
            $user 		= JFactory::getUser($data['user_id']);

            $options = array();
            $options['action'] = 'core.login.site';
            $app = JFactory::getApplication();
            $result = $app->triggerEvent('onUserLogin', array((array)$user, $options));
            if($params->get('mode') == 'duration'){
                $db = JFactory::getDbo();

        		// Create a new query object
        		$query = $db->getQuery(true);

        		$nowDate = new Datetime();
        		$currentUserID = JFactory::getUser()->id;

    			// Fields to update
    			$fields = array(
    					$db->quoteName('sendNotification') . ' = 0'
    			);

    			// Conditions for which records should be updated
    			$conditions = array(
    			    $db->quoteName('id') . ' = ' . $data['sub_id']
    			);

    			$query->update($db->quoteName('#__alkasubscriptions_subscription'))->set($fields)->where($conditions);

    			$db->setQuery($query);

    			$result = $db->execute();
            }
        }
    }
	public function onBeforeRender()
	{
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select(array('COUNT(id)'));
        $query->from($db->quoteName('#__alkasubscriptions_subscription'));
        $query->where($db->quoteName('idAddress') . ' IS NULL ');
        $query->where($db->quoteName('enabled') . ' = '.$db->quote(1));
        $query->where("(".$db->quoteName('endDate') . ' >= NOW() OR '.$db->quoteName('forever')." = ".$db->quote(1).")");

        $app = JFactory::getApplication();
        if ($app->isAdmin()){
            $db->setQuery($query);
            $count = $db->loadResult();
            if($count>0){
                JFactory::getApplication()->enqueueMessage('<a href="'.JRoute::_('index.php?option=com_alkasubscriptions').'">There\'s '.$count.' Alkasubscriptions  without addresses</a>', 'warning');
            }
        }else{
            $user = JFactory::getUser();
            $query->where($db->quoteName('idUser') . ' = '.$db->quote($user->id));
            $db->setQuery($query);

            $count = $db->loadResult();

            if($count>0 && $user->id){
                JFactory::getApplication()->enqueueMessage('You have '.$count.' Alkasubscriptions  without addresses', 'error');
            }
        }
	}
}
