<?php
defined('_JEXEC') or die;
class PlgSystemSalamandre extends JPlugin
{
    public function onAfterInitialise(){

        $jinput = JFactory::getApplication()->input;
        $redirectosalamandre = $jinput->get($this->params->get('varname'),false);

        if($redirectosalamandre){
            header("Location: ".$this->params->get('redirection'));
            die();
        }
    }
}
